"use strict";


Vue.component('analog-clock', {
	props: {
		utc: Boolean,
		shiftHours: Number,
		shiftMinutes: Number,
		staticTime: Date,
		handsSyncConfig: Object,
		aspectConfigBoard: Object,
		aspectConfigNumbers: Object,
		aspectConfigHoursHand: Object,
		aspectConfigMinutesHand: Object,
		aspectConfigSecondsHand: Object,
	},
	data: function () {
		return {
			clockNumbers: this.generateClockNumbers(),
			currentTime: new Date(),
			handsSyncDefault: {
				hours: 'minutes',
				minutes: 'minutes',
			},
			aspectDefaultBoard: {
				radius: '49.5%',
				fill: 'none',
				stroke: 'black',
			},
			aspectDefaultNumbers: {
				radius: '40',
			},
			aspectDefaultHoursHand: {
				radius: '31',
				stroke: 'black',
			},
			aspectDefaultMinutesHand: {
				radius: '46',
				stroke: 'black',
			},
			aspectDefaultSecondsHand: {
				radius: '48',
				stroke: 'black',
			},
			intervalId: null,
		};
	},
	computed: {
		hours: function () {
			return this.currentTime.getHours() % 12;
		},
		minutes: function () {
			return this.currentTime.getMinutes();
		},
		seconds: function () {
			return this.currentTime.getSeconds();
		},
		secondsInHour: function () {
			return this.minutes * 60 + this.seconds;
		},
		secondsInHalfDay: function () {
			return this.hours * 3600 + this.secondsInHour;
		},
		minutesInHalfDay: function () {
			return this.hours * 60 + this.minutes;
		},
		handsSync: function () {
			let defaults = Object.assign({}, this.handsSyncDefault);
			return Object.assign(defaults, this.handsSyncConfig);
		},
		hoursPositions: function () {
			switch (this.handsSync.hours) {
			case 'hours':
				return 12;
			case 'minutes':
				return 12 * 60;
			case 'seconds':
				return 12 * 60 * 60;
			default:
				return 'error';
			}
		},
		hoursPosition: function () {
			switch (this.handsSync.hours) {
			case 'hours':
				return this.hours;
			case 'minutes':
				return this.minutesInHalfDay;
			case 'seconds':
				return this.secondsInHalfDay;
			default:
				return 'error';
			}
		},
		minutesPositions: function () {
			switch (this.handsSync.minutes) {
			case 'minutes':
				return 60;
			case 'seconds':
				return 60 * 60;
			default:
				return 'error';
			}
		},
		minutesPosition: function () {
			switch (this.handsSync.minutes) {
			case 'minutes':
				return this.minutes;
			case 'seconds':
				return this.secondsInHour;
			default:
				return 'error';
			}
		},
		shift: function () {
			let date = new Date();
			let currentTime = date.getTime();
			let shiftMinutes = (this.shiftMinutes || 0) +
			                   (this.shiftHours * 60 || 0);
			date.setMinutes(date.getMinutes() + shiftMinutes);
			let shiftedTime = date.getTime();
			return shiftedTime - currentTime;
		},
		aspectBoard: function () {
			let defaults = Object.assign({}, this.aspectDefaultBoard);
			return Object.assign(defaults, this.aspectConfigBoard);
		},
		aspectNumbers: function () {
			let defaults = Object.assign({}, this.aspectDefaultNumbers);
			return Object.assign(defaults, this.aspectConfigNumbers);
		},
		aspectHoursHand: function () {
			let defaults = Object.assign({}, this.aspectDefaultHoursHand);
			return Object.assign(defaults, this.aspectConfigHoursHand);
		},
		aspectMinutesHand: function () {
			let defaults = Object.assign({}, this.aspectDefaultMinutesHand);
			return Object.assign(defaults, this.aspectConfigMinutesHand);
		},
		aspectSecondsHand: function () {
			let defaults = Object.assign({}, this.aspectDefaultSecondsHand);
			return Object.assign(defaults, this.aspectConfigSecondsHand);
		},
	},
	methods: {
		generateClockNumbers: function () {
			let clockNumbers = {};
			let angle = -60;  // Starting angle
			for (let i = 1; i < 13; i += 1) {
				clockNumbers[String(i)] = angle * ((2 * Math.PI) / 360);
				angle += 30;
			}
			return clockNumbers;
		},
		updateTimeLocal: function (dateArg = Date.now()) {
			this.currentTime = new Date(dateArg);
		},
		updateTimeUTC: function (dateArg = Date.now()) {
			let date = new Date(dateArg);
			// NOTE: The value of the Date object is changed for display
			// purposes (actual timezone cannot be changed).
			date.setMinutes(date.getMinutes() + date.getTimezoneOffset());
			this.currentTime = date;
		},
		updateTimeShift: function (dateArg = Date.now()) {
			let date = new Date(dateArg);
			// Shift is relative to UTC
			date.setMinutes(date.getMinutes() + date.getTimezoneOffset());
			date.setTime(date.getTime() + this.shift);
			this.currentTime = date;
		},
		setUpdateTime: function () {
			clearInterval(this.intervalId);
			if (this.staticTime) {
				return this.setTimeStatic();
			}
			if (this.utc == true) {
				return this.intervalId = setInterval(this.updateTimeUTC, 1000);
			}
			if (this.shift != 0) {
				return this.intervalId = setInterval(this.updateTimeShift, 1000);
			}
			return this.intervalId = setInterval(this.updateTimeLocal, 1000);
		},
		setTimeStatic: function () {
			if (this.utc == true) {
				return this.updateTimeUTC(this.staticTime);
			}
			if (this.shift != 0) {
				return this.updateTimeShift(this.staticTime);
			}
			return this.updateTimeLocal(this.staticTime);
		},
	},
	beforeMount: function () {
		this.setUpdateTime();
	},
	watch: {
		utc: 'setUpdateTime',
		shiftHours: 'setUpdateTime',
		shiftMinutes: 'setUpdateTime',
		staticTime: 'setUpdateTime',
	},
	template: `
		<svg
			xmlns="https://www.w3.org/2000/svg"
			viewBox="0 0 100 100"
		>
			<circle
				cx="50%" cy="50%"
				v-bind:r="aspectBoard.radius"
				v-bind:stroke="aspectBoard.stroke"
				v-bind:stroke-width="aspectBoard.strokeWidth"
				v-bind:stroke-opacity="aspectBoard.strokeOpacity"
				v-bind:fill="aspectBoard.fill"
				v-bind:fill-opacity="aspectBoard.fillOpacity"
			/>

			<text
				v-for="(angle, number) in clockNumbers"
				dominant-baseline="central"
				text-anchor="middle"
				v-bind:x="50 + (aspectNumbers.radius * Math.cos(angle))"
				v-bind:y="50 + (aspectNumbers.radius * Math.sin(angle))"
				v-bind:stroke="aspectNumbers.stroke"
				v-bind:stroke-width="aspectNumbers.strokeWidth"
				v-bind:stroke-opacity="aspectNumbers.strokeOpacity"
				v-bind:fill="aspectNumbers.fill"
				v-bind:fill-opacity="aspectNumbers.fillOpacity"
				v-bind:font-size="aspectNumbers.fontSize"
				v-bind:font-family="aspectNumbers.fontFamily"
				v-bind:font-weight="aspectNumbers.fontWeight"
			>{{ number }}</text>

			<clock-hand
				v-bind:positions="60"
				v-bind:position="seconds"
				v-bind:aspectConfig="aspectSecondsHand"
			/>

			<clock-hand
				v-bind:positions="minutesPositions"
				v-bind:position="minutesPosition"
				v-bind:aspectConfig="aspectMinutesHand"
			/>

			<clock-hand
				v-bind:positions="hoursPositions"
				v-bind:position="hoursPosition"
				v-bind:aspectConfig="aspectHoursHand"
			/>
		</svg>
	`,
});


Vue.component('clock-hand', {
	props: [
		'positions',
		'position',
		'aspectConfig',
	],
	data: function () {
		return {
			aspectDefault: {
				stroke: 'black',
			},
		};
	},
	computed: {
		positionAngles: function () {
			return this.generatePositionAnglesTable(this.positions);
		},
		aspect: function () {
			let defaults = Object.assign({}, this.aspectDefault);
			return Object.assign(defaults, this.aspectConfig);
		},
		handStart: function () {
			return 50 + (this.aspect.handOffset ? this.aspect.handOffset : 0);
		},
	},
	methods: {
		generatePositionAnglesTable: function (numPositions) {
			let positionAngles = {};
			for (let i = 0; i < numPositions; i += 1) {
				positionAngles[String(i)] = (i / numPositions) * 360;
			}
			return positionAngles;
		},
	},
	template: `
		<line
			x1="50%" x2="50%"
			v-bind:y1="handStart"
			v-bind:y2="(handStart - Number(aspect.radius)) + '%'"
			v-bind:stroke="aspect.stroke"
			v-bind:stroke-width="aspect.strokeWidth"
			v-bind:stroke-opacity="aspect.strokeOpacity"
			v-bind:transform="'rotate(' + positionAngles[position] + ' 50 50)'"
		/>
	`,
});
